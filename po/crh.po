# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# Reşat SABIQ <tilde.birlik@gmail.com>, 2009
msgid ""
msgstr ""
"Project-Id-Version: UKUI Desktop Environment\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-08-30 17:30+0800\n"
"PO-Revision-Date: 2015-12-14 10:18+0000\n"
"Last-Translator: monsta <monsta@inbox.ru>\n"
"Language-Team: Crimean Turkish (http://www.wiki.ukui.org/trans/ukui/UKUI/"
"language/crh/)\n"
"Language: crh\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: ../capplet/gsm-app-dialog.c:126
msgid "Select Command"
msgstr "Emir Sayla"

#: ../capplet/gsm-app-dialog.c:198
msgid "Add Startup Program"
msgstr "Başlanğıç Programı Ekle"

#: ../capplet/gsm-app-dialog.c:202
msgid "Edit Startup Program"
msgstr "Başlanğıç Programı Tarir Et"

#: ../capplet/gsm-app-dialog.c:487
msgid "The startup command cannot be empty"
msgstr "Başlanğıç emri boş olamaz"

#: ../capplet/gsm-app-dialog.c:493
msgid "The startup command is not valid"
msgstr "Başlanğıç emri keçerli degil"

#: ../capplet/gsm-properties-dialog.c:519
msgid "Enabled"
msgstr "Qabilleştirilgen"

#: ../capplet/gsm-properties-dialog.c:531
msgid "Icon"
msgstr "İşaretçik"

#: ../capplet/gsm-properties-dialog.c:543
msgid "Program"
msgstr "Program"

#: ../capplet/gsm-properties-dialog.c:742
msgid "Startup Applications Preferences"
msgstr "Başlanğıç Uyğulamaları Tercihleri"

#: ../capplet/gsp-app.c:274
msgid "No name"
msgstr "İsim yoq"

#: ../capplet/gsp-app.c:280
msgid "No description"
msgstr "Tasvirsiz"

#: ../capplet/main.c:36 ../ukui-session/main.c:642
msgid "Version of this application"
msgstr "Bu uyğulamanıñ sürümi"

#: ../capplet/main.c:52
msgid "Could not display help document"
msgstr "Yardım vesiqası kösterilamadı"

#: ../data/org.ukui.session.gschema.xml.in.h:1
msgid "Current session start time"
msgstr ""

#: ../data/org.ukui.session.gschema.xml.in.h:2
msgid "Unix time of the start of the current session."
msgstr ""

#: ../data/org.ukui.session.gschema.xml.in.h:3
msgid "Save sessions"
msgstr "Oturımlarnı saqla"

#: ../data/org.ukui.session.gschema.xml.in.h:4
msgid "If enabled, ukui-session will save the session automatically."
msgstr ""

#: ../data/org.ukui.session.gschema.xml.in.h:5
msgid "Logout prompt"
msgstr "Tışarı imzalanuv daveti"

#: ../data/org.ukui.session.gschema.xml.in.h:6
#, fuzzy
msgid "If enabled, ukui-session will prompt the user before ending a session."
msgstr ""
"Eger qabilleştirilgen ise, mate-session bir oturımnı qapatmadan evel "
"qullanıcığa sorar."

#: ../data/org.ukui.session.gschema.xml.in.h:7
msgid "Logout timeout"
msgstr ""

#: ../data/org.ukui.session.gschema.xml.in.h:8
msgid ""
"If logout prompt is enabled, this set the timeout in seconds before logout "
"automatically. If 0, automatic logout is disabled."
msgstr ""

#: ../data/org.ukui.session.gschema.xml.in.h:9
msgid "Time before session is considered idle"
msgstr "Oturım aylaq mulâaza etilmeden evelki vaqıt."

#: ../data/org.ukui.session.gschema.xml.in.h:10
msgid ""
"The number of minutes of inactivity before the session is considered idle."
msgstr "Oturım aylaq mulâaza etilmeden evel faaliyetsiz daqqa sayısı."

#: ../data/org.ukui.session.gschema.xml.in.h:11
msgid "Default session"
msgstr "Ög-belgilengen oturım"

#: ../data/org.ukui.session.gschema.xml.in.h:12
msgid "List of applications that are part of the default session."
msgstr "Ög-belgilengen oturımnıñ parçası olğan uyğulamalarnıñ cedveli."

#: ../data/org.ukui.session.gschema.xml.in.h:13
msgid "Required session components"
msgstr "Talap etilgen oturım komponentleri"

#: ../data/org.ukui.session.gschema.xml.in.h:14
msgid ""
"List of components that are required as part of the session. (Each element "
"names a key under \"/org/ukui/desktop/session/required_components\"). The "
"Startup Applications preferences tool will not normally allow users to "
"remove a required component from the session, and the session manager will "
"automatically add the required components back to the session at login time "
"if they do get removed."
msgstr ""

#: ../data/org.ukui.session.gschema.xml.in.h:15
msgid "Control gnome compatibility component startup"
msgstr ""

#: ../data/org.ukui.session.gschema.xml.in.h:16
msgid "Control which compatibility components to start."
msgstr ""

#: ../data/org.ukui.session.gschema.xml.in.h:17
#: ../data/ukui-wm.desktop.in.in.h:1
msgid "Window Manager"
msgstr "Pencere İdarecisi"

#: ../data/org.ukui.session.gschema.xml.in.h:18
msgid ""
"The window manager is the program that draws the title bar and borders "
"around windows, and allows you to move and resize windows."
msgstr ""
"Pencere idarecisi, pencerelerniñ serlevalarını ve etrafındaki sıñırlarnı "
"sızğan ve pencerelerni avuştırmağa ve kene ölçülendirmege imkân temin etken "
"programdır."

#: ../data/org.ukui.session.gschema.xml.in.h:19
msgid "Panel"
msgstr "Panel"

#: ../data/org.ukui.session.gschema.xml.in.h:20
msgid ""
"The panel provides the bar at the top or bottom of the screen containing "
"menus, the window list, status icons, the clock, etc."
msgstr ""
"Panel, ekrannıñ üstünde ya da tübünde menüler, pencere cedveli, durum "
"işaretçikleri, saat vs. ihtiva etken çubuq temin eter."

#: ../data/org.ukui.session.gschema.xml.in.h:21
msgid "File Manager"
msgstr "Dosye İdarecisi"

#: ../data/org.ukui.session.gschema.xml.in.h:22
msgid ""
"The file manager provides the desktop icons and allows you to interact with "
"your saved files."
msgstr ""
"Dosye idarecisi, masaüstü işaretçiklerini ve saqlanğan dosyeleriñiz ile "
"tesirleşim imkânını temin eter."

#: ../data/org.ukui.session.gschema.xml.in.h:23
msgid "Dock"
msgstr ""

#: ../data/org.ukui.session.gschema.xml.in.h:24
msgid ""
"A dock provides a dockable area, similar to a panel, for launching and "
"switching applications."
msgstr ""

#: ../data/ukui.desktop.in.h:1
msgid "UKUI"
msgstr ""

#: ../data/ukui.desktop.in.h:2
#, fuzzy
msgid "This session logs you into UKUI"
msgstr "Bu oturım sizni MATE'ğa içeri imzalandırır"

#: ../data/gsm-inhibit-dialog.ui.h:1
msgid "<b>Some programs are still running:</b>"
msgstr "<b>Bazı programlar ale çapmaqta:</b>"

#: ../data/gsm-inhibit-dialog.ui.h:2 ../ukui-session/gsm-inhibit-dialog.c:634
msgid ""
"Waiting for the program to finish.  Interrupting the program may cause you "
"to lose work."
msgstr ""

#: ../data/ukui-session-properties.desktop.in.in.h:1
msgid "Startup Applications"
msgstr "Başlanğıç Uyğulamaları"

#: ../data/ukui-session-properties.desktop.in.in.h:2
msgid "Choose what applications to start when you log in"
msgstr "İçeri imzalanğan soñ başlatılacaq uyğulamalarnı saylañız"

#: ../data/session-properties.ui.h:1
msgid "Additional startup _programs:"
msgstr "Ek başlanğıç _programları:"

#: ../data/session-properties.ui.h:2
msgid "Startup Programs"
msgstr "Başlanğıç Uyğulamaları"

#: ../data/session-properties.ui.h:3
msgid "_Automatically remember running applications when logging out"
msgstr "Tışarı imzalanğanda çapmaqta olğan uyğulamalarnı _öz-özünden hatırla"

#: ../data/session-properties.ui.h:4
msgid "_Remember Currently Running Application"
msgstr "_Al-azırda Çapmaqta Olğan Uyğulamanı Hatırla"

#: ../data/session-properties.ui.h:5
msgid "Options"
msgstr "İhtiyariyat"

#: ../data/session-properties.ui.h:6
msgid "Browse…"
msgstr ""

#: ../data/session-properties.ui.h:7
msgid "Comm_ent:"
msgstr "_Şerh:"

#: ../data/session-properties.ui.h:8
msgid "Co_mmand:"
msgstr "_Emir:"

#: ../data/session-properties.ui.h:9
msgid "_Name:"
msgstr "_İsim:"

#: ../egg/eggdesktopfile.c:152
#, c-format
msgid "File is not a valid .desktop file"
msgstr "Dosye keçerli bir .desktop dosyesi degil"

#: ../egg/eggdesktopfile.c:172
#, c-format
msgid "Unrecognized desktop file Version '%s'"
msgstr "Tanılmağan masaüstü dosyesi Sürümi '%s'"

#: ../egg/eggdesktopfile.c:956
#, c-format
msgid "Starting %s"
msgstr "%s Başlatıla"

#: ../egg/eggdesktopfile.c:1097
#, c-format
msgid "Application does not accept documents on command line"
msgstr "Uyğulama emir satrında vesiqalarnı qabul etmey"

#: ../egg/eggdesktopfile.c:1165
#, c-format
msgid "Unrecognized launch option: %d"
msgstr "Tanılmağan fırlatma ihtiyariyatı: %d"

#: ../egg/eggdesktopfile.c:1363
#, c-format
msgid "Can't pass document URIs to a 'Type=Link' desktop entry"
msgstr "Bir 'Tür=İlişim' masaüstü kirildisine vesiqa URI'leri keçirilamaz"

#: ../egg/eggdesktopfile.c:1384
#, c-format
msgid "Not a launchable item"
msgstr "Fırlatılabilir bir unsur degil"

#: ../egg/eggsmclient.c:226
msgid "Disable connection to session manager"
msgstr "Oturım idarecisine bağlantını ğayrı qabilleştir"

#: ../egg/eggsmclient.c:229
msgid "Specify file containing saved configuration"
msgstr "Saqlanğan ayarlamanı ihtiva etken dosyeni belirtiñiz"

#: ../egg/eggsmclient.c:229
msgid "FILE"
msgstr "DOSYE"

#: ../egg/eggsmclient.c:232
msgid "Specify session management ID"
msgstr "Oturım idaresi kimligini belirtiñiz"

#: ../egg/eggsmclient.c:232
msgid "ID"
msgstr "Kimlik"

#: ../egg/eggsmclient.c:253
msgid "Session management options:"
msgstr "Oturım idaresi ihtiyariyatı:"

#: ../egg/eggsmclient.c:254
msgid "Show session management options"
msgstr "Oturım idaresi ihtiyariyatını köster"

#: ../ukui-session/gsm-inhibit-dialog.c:252
#, c-format
msgid "Icon '%s' not found"
msgstr "'%s' işaretçigi tapılamadı"

#: ../ukui-session/gsm-inhibit-dialog.c:582
msgid "Unknown"
msgstr "Bilinmey"

#: ../ukui-session/gsm-inhibit-dialog.c:633
msgid "A program is still running:"
msgstr "Bir program ale çapmaqta:"

#: ../ukui-session/gsm-inhibit-dialog.c:637
msgid "Some programs are still running:"
msgstr "Bazı programlar ale çapmaqta:"

#: ../ukui-session/gsm-inhibit-dialog.c:638
msgid ""
"Waiting for programs to finish.  Interrupting these programs may cause you "
"to lose work."
msgstr ""
"Programlarnıñ tamamlanuvı içün beklene.  Bu programlarnıñ kestirilüvi yapqan "
"olğanıñız işlerni ğayıp etüviñizge sebep ola bilir."

#: ../ukui-session/gsm-inhibit-dialog.c:868
msgid "Switch User Anyway"
msgstr "Kene de Qullanıcını Deñiştir"

#: ../ukui-session/gsm-inhibit-dialog.c:871
msgid "Log Out Anyway"
msgstr ""

#: ../ukui-session/gsm-inhibit-dialog.c:874
msgid "Suspend Anyway"
msgstr "Kene de Sarqıt"

#: ../ukui-session/gsm-inhibit-dialog.c:877
msgid "Hibernate Anyway"
msgstr "Kene de Yuqlat"

#: ../ukui-session/gsm-inhibit-dialog.c:880
msgid "Shut Down Anyway"
msgstr ""

#: ../ukui-session/gsm-inhibit-dialog.c:883
msgid "Reboot Anyway"
msgstr "Kene de Yañıdan Başlat"

#: ../ukui-session/gsm-inhibit-dialog.c:891
msgid "Lock Screen"
msgstr "Ekrannı Kilitle"

#: ../ukui-session/gsm-inhibit-dialog.c:894
msgid "Cancel"
msgstr "Vazgeç"

#: ../ukui-session/gsm-logout-dialog.c:477
#, c-format
msgid "You will be automatically logged out in %d second"
msgid_plural "You will be automatically logged out in %d seconds"
msgstr[0] ""

#: ../ukui-session/gsm-logout-dialog.c:485
#, c-format
msgid "This system will be automatically shut down in %d second"
msgid_plural "This system will be automatically shut down in %d seconds"
msgstr[0] ""

#: ../ukui-session/gsm-logout-dialog.c:531
#, c-format
msgid "You are currently logged in as \"%s\"."
msgstr "Al-azırda \"%s\" olaraq içeri imzalanğansıñız."

#: ../ukui-session/gsm-logout-dialog.c:664
msgid "Log out of this system now?"
msgstr "Bu sistemden şimdi çıqılsınmı?"

#: ../ukui-session/gsm-logout-dialog.c:674
#, fuzzy
msgid "Switch User"
msgstr "_Qullanıcı Almaştır"

#: ../ukui-session/gsm-logout-dialog.c:687
#, fuzzy
msgid "Log Out"
msgstr "_Tışarı İmzalan"

#: ../ukui-session/gsm-logout-dialog.c:699
msgid "Shut down this system now?"
msgstr "Bu sistem şimdi qapatılsınmı?"

#: ../ukui-session/gsm-logout-dialog.c:709
#, fuzzy
msgid "Suspend"
msgstr "_Sarqıt"

#: ../ukui-session/gsm-logout-dialog.c:722
#, fuzzy
msgid "Hibernate"
msgstr "_Yuqlat"

#: ../ukui-session/gsm-logout-dialog.c:735
#, fuzzy
msgid "Restart"
msgstr "_Kene Başlat"

#: ../ukui-session/gsm-logout-dialog.c:749
#, fuzzy
msgid "Shut Down"
msgstr "_Qapat"

#: ../ukui-session/gsm-manager.c:1417 ../ukui-session/gsm-manager.c:2136
msgid "Not responding"
msgstr "Cevap bermey"

#. It'd be really surprising to reach this code: if we're here,
#. * then the XSMP client already has set several XSMP
#. * properties. But it could still be that SmProgram is not set.
#.
#: ../ukui-session/gsm-xsmp-client.c:566
msgid "Remembered Application"
msgstr ""

#: ../ukui-session/gsm-xsmp-client.c:1204
msgid "This program is blocking logout."
msgstr ""

#: ../ukui-session/gsm-xsmp-server.c:325
msgid ""
"Refusing new client connection because the session is currently being shut "
"down\n"
msgstr ""
"Oturım şu ande qapatılmaqta olğandan dolayı yañı müşteri bağlantısı red "
"etile\n"

#: ../ukui-session/gsm-xsmp-server.c:587
#, c-format
msgid "Could not create ICE listening socket: %s"
msgstr "ICE diñlev oyuğı (soketi) icat etilamadı: %s"

#. Oh well, no X for you!
#: ../ukui-session/gsm-util.c:356
#, c-format
msgid "Unable to start login session (and unable to connect to the X server)"
msgstr "İçeri imzalanuv oturımı başlatılamay (ve X sunucısına bağlanılamay)"

#: ../ukui-session/main.c:639
msgid "Override standard autostart directories"
msgstr "Standart avto-başlanğıç fihristleriniñ üstünden ayda"

#: ../ukui-session/main.c:640
msgid "Enable debugging code"
msgstr "Arızasızlandıruv kodunı qabilleştir"

#: ../ukui-session/main.c:641
msgid "Do not load user-specified applications"
msgstr "Qullanıcı tarafından belirtilgen programlarnı yükleme"

#: ../ukui-session/main.c:643
msgid "Show the keyboard shortcuts"
msgstr ""

#: ../ukui-session/main.c:663
#, fuzzy
msgid " - the UKUI session manager"
msgstr " - MATE oturım idarecisi"

#: ../ukui-session/uksm-shortcuts-dialog.c:350
#, c-format
msgid "This window will be automatically closed in %d second"
msgid_plural "This window will be automatically closed in %d seconds"
msgstr[0] ""

#: ../ukui-session/uksm-shortcuts-dialog.c:466
msgid "Keyboard Shortcuts"
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:468
#, fuzzy
msgid "Application"
msgstr "Başlanğıç Uyğulamaları"

#: ../ukui-session/uksm-shortcuts-dialog.c:469
msgid "Super"
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:470
msgid "Open start menu."
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:471
msgid "Ctrl+Alt+T"
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:472
msgid "Open termianl."
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:473
msgid "Ctrl+Alt+D"
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:474
msgid "Show desktop."
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:475
msgid "Ctrl+Alt+L"
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:476
#, fuzzy
msgid "Lock screen."
msgstr "Ekrannı Kilitle"

#. Separator
#: ../ukui-session/uksm-shortcuts-dialog.c:479
#: ../ukui-session/uksm-shortcuts-dialog.c:500
msgid "  "
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:481
msgid "Screenshot"
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:482
msgid "Print"
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:483
msgid "Take a full-screen screenshot."
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:484
msgid "Ctrl+Print"
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:485
msgid "Take a screenshot of the current window."
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:486
msgid "Shift+Print"
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:487
msgid "Select area to grab."
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:489
msgid "Switching"
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:490
msgid "Alt+Tab"
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:491
#, fuzzy
msgid "Switches between applications."
msgstr "Başlanğıç Uyğulamaları"

#: ../ukui-session/uksm-shortcuts-dialog.c:492
msgid "Alt+Shift+Tab"
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:493
msgid "Switches between applications in reverse order."
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:494
msgid "Super_L+P/F3/F7"
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:495
msgid "Switches between display."
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:496
msgid "Ctrl+Alt+ArrowKeys"
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:497
msgid "Switches between workspaces."
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:502
msgid "Windows"
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:503
msgid "Alt+F9"
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:504
msgid "Maximize the current window."
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:505
msgid "Alt+F10"
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:506
msgid "Minimize the current window."
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:507
msgid "Alt+F5"
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:508
msgid "Cancel maximize the current window."
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:509
msgid "Alt+F4"
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:510
#, fuzzy
msgid "Close the current window."
msgstr "Ağımdaki oturım ismini tesbit et"

#: ../tools/ukui-session-save.c:70
msgid "Log out"
msgstr "Tışarı imzalan"

#: ../tools/ukui-session-save.c:71
msgid "Log out, ignoring any existing inhibitors"
msgstr "Tışarı imzalan, mevcut er türlü sedlerni ihmal eterek"

#: ../tools/ukui-session-save.c:72
msgid "Show logout dialog"
msgstr "Tışarı imzalanuv dialogını köster"

#: ../tools/ukui-session-save.c:73
msgid "Show shutdown dialog"
msgstr "Qapatuv dialogını köster"

#: ../tools/ukui-session-save.c:74
msgid "Use dialog boxes for errors"
msgstr "Hatalar içün dialog qutularını qullan"

#. deprecated options
#: ../tools/ukui-session-save.c:76
msgid "Set the current session name"
msgstr "Ağımdaki oturım ismini tesbit et"

#: ../tools/ukui-session-save.c:76
msgid "NAME"
msgstr "İSİM"

#: ../tools/ukui-session-save.c:77
msgid "Kill session"
msgstr "Oturımnı öldür"

#: ../tools/ukui-session-save.c:78
msgid "Do not require confirmation"
msgstr "Tasdiq talap etme"

#. Okay, it wasn't the upgrade case, so now we can give up.
#: ../tools/ukui-session-save.c:136 ../tools/ukui-session-save.c:151
msgid "Could not connect to the session manager"
msgstr "Oturım idarecisine bağlanılamadı"

#: ../tools/ukui-session-save.c:277
msgid "Program called with conflicting options"
msgstr "Program çatışqan ihtiyariyat ile çağırıldı"

#: ../tools/ukui-session-inhibit.c:116
#, c-format
msgid ""
"%s [OPTION...] COMMAND\n"
"\n"
"Execute COMMAND while inhibiting some session functionality.\n"
"\n"
"  -h, --help        Show this help\n"
"  --version         Show program version\n"
"  --app-id ID       The application id to use\n"
"                    when inhibiting (optional)\n"
"  --reason REASON   The reason for inhibiting (optional)\n"
"  --inhibit ARG     Things to inhibit, colon-separated list of:\n"
"                    logout, switch-user, suspend, idle, automount\n"
"\n"
"If no --inhibit option is specified, idle is assumed.\n"
msgstr ""

#: ../tools/ukui-session-inhibit.c:170 ../tools/ukui-session-inhibit.c:180
#: ../tools/ukui-session-inhibit.c:190
#, c-format
msgid "%s requires an argument\n"
msgstr ""

#: ../tools/ukui-session-inhibit.c:226
#, c-format
msgid "Failed to execute %s\n"
msgstr ""

#~ msgid "MATE"
#~ msgstr "MATE"
