# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER, 2005
# Siranush <ss.m.95@mail.ru>, 2015-2016
msgid ""
msgstr ""
"Project-Id-Version: UKUI Desktop Environment\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-08-30 17:30+0800\n"
"PO-Revision-Date: 2016-01-18 16:16+0000\n"
"Last-Translator: Siranush <ss.m.95@mail.ru>\n"
"Language-Team: Armenian (http://www.wiki.ukui.org/trans/ukui/UKUI/language/"
"hy/)\n"
"Language: hy\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../capplet/gsm-app-dialog.c:126
msgid "Select Command"
msgstr "Ընտրել Հրաման"

#: ../capplet/gsm-app-dialog.c:198
msgid "Add Startup Program"
msgstr "Ավելացնել գործարկման ծրագիր"

#: ../capplet/gsm-app-dialog.c:202
msgid "Edit Startup Program"
msgstr "Խմբագրել գործարկման ծրագիրը"

#: ../capplet/gsm-app-dialog.c:487
msgid "The startup command cannot be empty"
msgstr "Գործարկման հրահանգը չի կարող դատարկ լինել"

#: ../capplet/gsm-app-dialog.c:493
msgid "The startup command is not valid"
msgstr "Գործարկման հրամանը վավերական չէ"

#: ../capplet/gsm-properties-dialog.c:519
msgid "Enabled"
msgstr "Միացված"

#: ../capplet/gsm-properties-dialog.c:531
msgid "Icon"
msgstr "Պատկեր"

#: ../capplet/gsm-properties-dialog.c:543
msgid "Program"
msgstr "Ծրագիր"

#: ../capplet/gsm-properties-dialog.c:742
msgid "Startup Applications Preferences"
msgstr "Մեկնարկի դիմումների արտոնությունները"

#: ../capplet/gsp-app.c:274
msgid "No name"
msgstr "Անանուն"

#: ../capplet/gsp-app.c:280
msgid "No description"
msgstr "Ոչ մի Նկարագրություն"

#: ../capplet/main.c:36 ../ukui-session/main.c:642
msgid "Version of this application"
msgstr "Այս դիմումի տարբերակը"

#: ../capplet/main.c:52
msgid "Could not display help document"
msgstr "Չի հաջողվել ցույց տալ օգնության փաստաթուղթը"

#: ../data/org.ukui.session.gschema.xml.in.h:1
msgid "Current session start time"
msgstr "Ընթացիկ աշխատաշրջանի սկսելու ժամանակ"

#: ../data/org.ukui.session.gschema.xml.in.h:2
msgid "Unix time of the start of the current session."
msgstr "Unix ժամանակը մեկնարկեց ընթացիկ նստաշրջանը։"

#: ../data/org.ukui.session.gschema.xml.in.h:3
msgid "Save sessions"
msgstr "Պահպանել սեանսները"

#: ../data/org.ukui.session.gschema.xml.in.h:4
#, fuzzy
msgid "If enabled, ukui-session will save the session automatically."
msgstr "Եթե միացված է,  ապա mate նստաշրջանը նիստը կպահպանի ինքնաբերաբար։"

#: ../data/org.ukui.session.gschema.xml.in.h:5
msgid "Logout prompt"
msgstr "Համակարգից դուրս գալու հուշում"

#: ../data/org.ukui.session.gschema.xml.in.h:6
#, fuzzy
msgid "If enabled, ukui-session will prompt the user before ending a session."
msgstr ""
"Միացնելու դեպքում գնոմ–ենթահամակարգը գործարկողին հուշում է սեանսի ավարտի "
"մասին"

#: ../data/org.ukui.session.gschema.xml.in.h:7
msgid "Logout timeout"
msgstr "Ավարտի սահմանված ժամանակի ավարտը"

#: ../data/org.ukui.session.gschema.xml.in.h:8
msgid ""
"If logout prompt is enabled, this set the timeout in seconds before logout "
"automatically. If 0, automatic logout is disabled."
msgstr ""
"Եթե դուրս գալու հուշումը միացված է, ապա այս սահմանում է ժամանակի ավարտը "
"վարկյաններով, նախքան ինքնաբերաբար դուրս գալը։ Եթե 0, ապա ավտոմատ դուրս գալը "
"անջատված է։"

#: ../data/org.ukui.session.gschema.xml.in.h:9
msgid "Time before session is considered idle"
msgstr "Ժամանակը նախքան նիստը համարվում է պարապ"

#: ../data/org.ukui.session.gschema.xml.in.h:10
msgid ""
"The number of minutes of inactivity before the session is considered idle."
msgstr "Անգործության րոպեների քանակը նիստից առաջ համարվում է պարապ։"

#: ../data/org.ukui.session.gschema.xml.in.h:11
msgid "Default session"
msgstr "Հիմնական աշխատաշրջան"

#: ../data/org.ukui.session.gschema.xml.in.h:12
msgid "List of applications that are part of the default session."
msgstr "Կիրառական ծրագրերի ցանկը, որոնք հաստատուն նիստի մաս են կազմում։"

#: ../data/org.ukui.session.gschema.xml.in.h:13
msgid "Required session components"
msgstr "Աշխատաշրջանի պահանջվող բազադրիչներ"

#: ../data/org.ukui.session.gschema.xml.in.h:14
#, fuzzy
msgid ""
"List of components that are required as part of the session. (Each element "
"names a key under \"/org/ukui/desktop/session/required_components\"). The "
"Startup Applications preferences tool will not normally allow users to "
"remove a required component from the session, and the session manager will "
"automatically add the required components back to the session at login time "
"if they do get removed."
msgstr ""
"Բաղադրիչների ցանկը, որոնք անհրաժեշտ են որպես նիստի մաս։ (Յուրաքանչյուր տարրի "
"անուները բանալիի տակ \"/org/mate/desktop/session/required_components\")։ "
"Գործարկման Ծրագրերի նախապատվությունների գործիքը սովորաբար թույլ չի տալիս "
"օգտվողներին հեռացնել պահանջվող նիստի բաղադրիչը և նիստ կառավարիչը "
"ինքնաբերաբար ավելացնում է պահանջվող բաղադրիչները դեպի հետ նստաշրջանի մուտքի "
"ժամանակ, եթե նրանք չեն հեռացվել։"

#: ../data/org.ukui.session.gschema.xml.in.h:15
msgid "Control gnome compatibility component startup"
msgstr "gnome վերահսկիչ համատեղելիության բաղկացուցիչ մեկնարկը"

#: ../data/org.ukui.session.gschema.xml.in.h:16
msgid "Control which compatibility components to start."
msgstr "Վերահսկիչ որը սկսում է բաղադրիչների համատեղելիությունը:"

#: ../data/org.ukui.session.gschema.xml.in.h:17
#: ../data/ukui-wm.desktop.in.in.h:1
msgid "Window Manager"
msgstr "Պատուհանի մենեջեր"

#: ../data/org.ukui.session.gschema.xml.in.h:18
msgid ""
"The window manager is the program that draws the title bar and borders "
"around windows, and allows you to move and resize windows."
msgstr ""
"Պատուհանային կառավարիչը ծրագիր է, որը պատկերում է վերնագիր բարը և "
"պատուհանների սահմանները շուրջը, և թույլ է տալիս Ձեզ տեղափոխել և չափափոխել "
"պատուհանները։"

#: ../data/org.ukui.session.gschema.xml.in.h:19
msgid "Panel"
msgstr "Վահանակ"

#: ../data/org.ukui.session.gschema.xml.in.h:20
msgid ""
"The panel provides the bar at the top or bottom of the screen containing "
"menus, the window list, status icons, the clock, etc."
msgstr ""
"Վահանակը ապահովում է բարը էկրանի վերևի կամ ներքևի մասում, որը պարունակում "
"ընտրացանկերը, պատուհանի ցուցակը, կարգավիճակի պատկերանշանները, ժամացույցը և "
"այլն։"

#: ../data/org.ukui.session.gschema.xml.in.h:21
msgid "File Manager"
msgstr "Ֆայլի Կառավարիչ"

#: ../data/org.ukui.session.gschema.xml.in.h:22
msgid ""
"The file manager provides the desktop icons and allows you to interact with "
"your saved files."
msgstr ""
"Ֆայլային կառավարիչը ապահովում է աշխատասեղանի պատկերանշանները և թույլ է տալիս "
"Ձեզ փոխներգործել ձեր պահպանած ֆայլերի հետ։"

#: ../data/org.ukui.session.gschema.xml.in.h:23
msgid "Dock"
msgstr "Կտրել"

#: ../data/org.ukui.session.gschema.xml.in.h:24
msgid ""
"A dock provides a dockable area, similar to a panel, for launching and "
"switching applications."
msgstr ""
"Կտրելը ապահովումէ կտրելի տարածք, որը նման է վահանակի, գործարկելու և "
"կիրառական ծրագրերի փոխանջատելու համար։"

#: ../data/ukui.desktop.in.h:1
msgid "UKUI"
msgstr ""

#: ../data/ukui.desktop.in.h:2
#, fuzzy
msgid "This session logs you into UKUI"
msgstr "Այս նիստը գրանցում է քեզ MATE֊ի մեջ"

#: ../data/gsm-inhibit-dialog.ui.h:1
msgid "<b>Some programs are still running:</b>"
msgstr "<b>Որոշ ծրագրեր դեռ աշխատում են:</b>"

#: ../data/gsm-inhibit-dialog.ui.h:2 ../ukui-session/gsm-inhibit-dialog.c:634
msgid ""
"Waiting for the program to finish.  Interrupting the program may cause you "
"to lose work."
msgstr ""
"Սպասում ծրագրի ավարտի համար։ Ծրագրի ընդհատումը կարող է առաջացնել աշխատանքի "
"կորուստ։"

#: ../data/ukui-session-properties.desktop.in.in.h:1
msgid "Startup Applications"
msgstr "Գործարկման Դիմումներ"

#: ../data/ukui-session-properties.desktop.in.in.h:2
msgid "Choose what applications to start when you log in"
msgstr "Ընտրեք, թե ինչ ծրագրեր են սկսվելու, երբ դուք մուտք գործեք"

#: ../data/session-properties.ui.h:1
msgid "Additional startup _programs:"
msgstr "Լրացուցիչ գործարկման -ծրագրեր"

#: ../data/session-properties.ui.h:2
msgid "Startup Programs"
msgstr "Գործարկման ծրագրեր"

#: ../data/session-properties.ui.h:3
msgid "_Automatically remember running applications when logging out"
msgstr "_Ավտոմատ կերպով հիշել ընթացող դիմումները, երբ դուս է գալիս"

#: ../data/session-properties.ui.h:4
msgid "_Remember Currently Running Application"
msgstr "_Հիշել Ներկայումս Գործարկվող Դիմումը"

#: ../data/session-properties.ui.h:5
msgid "Options"
msgstr "Տարբերակներ"

#: ../data/session-properties.ui.h:6
msgid "Browse…"
msgstr "Թերթել․․․"

#: ../data/session-properties.ui.h:7
msgid "Comm_ent:"
msgstr "Մեկնաբանություն։"

#: ../data/session-properties.ui.h:8
msgid "Co_mmand:"
msgstr "Հր_ահանգ։"

#: ../data/session-properties.ui.h:9
msgid "_Name:"
msgstr "_Անուն։"

#: ../egg/eggdesktopfile.c:152
#, c-format
msgid "File is not a valid .desktop file"
msgstr "Ֆայլը վավեր չէ .աշխատանքային սեղան ֆայլ"

#: ../egg/eggdesktopfile.c:172
#, c-format
msgid "Unrecognized desktop file Version '%s'"
msgstr "Չճանաչված աշխատանքային ֆայլի տարբերակ '%s'"

#: ../egg/eggdesktopfile.c:956
#, c-format
msgid "Starting %s"
msgstr "Գործարկում %s"

#: ../egg/eggdesktopfile.c:1097
#, c-format
msgid "Application does not accept documents on command line"
msgstr "Ծրագիրը փաստաթուղթ չի ընդունում հրամանի տողում "

#: ../egg/eggdesktopfile.c:1165
#, c-format
msgid "Unrecognized launch option: %d"
msgstr "Չճանաչված գործարկչի ընտրացանկ. %d"

#: ../egg/eggdesktopfile.c:1363
#, c-format
msgid "Can't pass document URIs to a 'Type=Link' desktop entry"
msgstr ""
"Չի հաջողվել անցկացնել փաստաթղթի URI–ները աշխատաքային սեղանի մուտքի "
"'Type=Link'–ին "

#: ../egg/eggdesktopfile.c:1384
#, c-format
msgid "Not a launchable item"
msgstr "Չգործարկվող տարր"

#: ../egg/eggsmclient.c:226
msgid "Disable connection to session manager"
msgstr "Անջատել ենթահամակարգի կառավարչի հետ կապը"

#: ../egg/eggsmclient.c:229
msgid "Specify file containing saved configuration"
msgstr "Պահպանված կոնֆիգուրացիա պարունակող ֆայլը նշել"

#: ../egg/eggsmclient.c:229
msgid "FILE"
msgstr "ՖԱՅԼ"

#: ../egg/eggsmclient.c:232
msgid "Specify session management ID"
msgstr "Հ ատկորոշել ենթահամակարգի կառավարչի ID"

#: ../egg/eggsmclient.c:232
msgid "ID"
msgstr "ID"

#: ../egg/eggsmclient.c:253
msgid "Session management options:"
msgstr "Սեսիայի կառավարման տարբերակներ․"

#: ../egg/eggsmclient.c:254
msgid "Show session management options"
msgstr "Ցույց տալ սեսիայի կառավարման ընտրացանկը"

#: ../ukui-session/gsm-inhibit-dialog.c:252
#, c-format
msgid "Icon '%s' not found"
msgstr "Պատկերակը '%s' չի գտնվել"

#: ../ukui-session/gsm-inhibit-dialog.c:582
msgid "Unknown"
msgstr "Անհայտ"

#: ../ukui-session/gsm-inhibit-dialog.c:633
msgid "A program is still running:"
msgstr "Ծրագիրը դեռ աշխատում է:"

#: ../ukui-session/gsm-inhibit-dialog.c:637
msgid "Some programs are still running:"
msgstr "Որոշ ծրագրեր դեռ աշխատում են:"

#: ../ukui-session/gsm-inhibit-dialog.c:638
msgid ""
"Waiting for programs to finish.  Interrupting these programs may cause you "
"to lose work."
msgstr ""
"Սպասում ծրագրի ավարտի համար։ Այդ ծրագրերը ընդհատելը կարող են առաջացնել "
"աշխատանքի կորուստ։"

#: ../ukui-session/gsm-inhibit-dialog.c:868
msgid "Switch User Anyway"
msgstr "Ամեն դեպքում Փոխանջատել Օգտատիրոջը"

#: ../ukui-session/gsm-inhibit-dialog.c:871
msgid "Log Out Anyway"
msgstr "Դուրս գալ Ամեն դեպքում"

#: ../ukui-session/gsm-inhibit-dialog.c:874
msgid "Suspend Anyway"
msgstr "Կասեցնել Ամեն դեպքում"

#: ../ukui-session/gsm-inhibit-dialog.c:877
msgid "Hibernate Anyway"
msgstr "Ձմեռել Ամեն դեպքում"

#: ../ukui-session/gsm-inhibit-dialog.c:880
msgid "Shut Down Anyway"
msgstr "Անջատել Ամեն դեպքում"

#: ../ukui-session/gsm-inhibit-dialog.c:883
msgid "Reboot Anyway"
msgstr "Վերագործարկել Ամեն դեպքում"

#: ../ukui-session/gsm-inhibit-dialog.c:891
msgid "Lock Screen"
msgstr "Արգելափակել էկրանը"

#: ../ukui-session/gsm-inhibit-dialog.c:894
msgid "Cancel"
msgstr "Չեղարկել"

#: ../ukui-session/gsm-logout-dialog.c:477
#, c-format
msgid "You will be automatically logged out in %d second"
msgid_plural "You will be automatically logged out in %d seconds"
msgstr[0] "Դուք պետք է ավտոմատ կերպով դուրս գաք %d վայրկյանում"
msgstr[1] "Դուք պետք է ավտոմատ կերպով դուրս գաք %d վայրկյաններում"

#: ../ukui-session/gsm-logout-dialog.c:485
#, c-format
msgid "This system will be automatically shut down in %d second"
msgid_plural "This system will be automatically shut down in %d seconds"
msgstr[0] "Այս համակարգը կարող է ավտոմատ կերպով անջատվել %d վայրկյանում"
msgstr[1] "Այս համակարգը կարող է ավտոմատ կերպով անջատվել %d վայրկյաններում"

#: ../ukui-session/gsm-logout-dialog.c:531
#, c-format
msgid "You are currently logged in as \"%s\"."
msgstr "Դուք այժմ մուտք եք գործել, որպես \"%s\"."

#: ../ukui-session/gsm-logout-dialog.c:664
msgid "Log out of this system now?"
msgstr "Հիմա Դուրս գա՞լ այս համակարգից"

#: ../ukui-session/gsm-logout-dialog.c:674
#, fuzzy
msgid "Switch User"
msgstr "_Փոխել Օգտագործողին"

#: ../ukui-session/gsm-logout-dialog.c:687
#, fuzzy
msgid "Log Out"
msgstr "_Դուրս գալ"

#: ../ukui-session/gsm-logout-dialog.c:699
msgid "Shut down this system now?"
msgstr "Հիմա անջատե՞լ այս համակարգը"

#: ../ukui-session/gsm-logout-dialog.c:709
#, fuzzy
msgid "Suspend"
msgstr "Հետաձգել"

#: ../ukui-session/gsm-logout-dialog.c:722
#, fuzzy
msgid "Hibernate"
msgstr "_Ձմեռել"

#: ../ukui-session/gsm-logout-dialog.c:735
#, fuzzy
msgid "Restart"
msgstr "_Վերսկսել"

#: ../ukui-session/gsm-logout-dialog.c:749
#, fuzzy
msgid "Shut Down"
msgstr "_Անջատել"

#: ../ukui-session/gsm-manager.c:1417 ../ukui-session/gsm-manager.c:2136
msgid "Not responding"
msgstr "Չի արձագանքում"

#. It'd be really surprising to reach this code: if we're here,
#. * then the XSMP client already has set several XSMP
#. * properties. But it could still be that SmProgram is not set.
#.
#: ../ukui-session/gsm-xsmp-client.c:566
msgid "Remembered Application"
msgstr "Հիշված Դիմում"

#: ../ukui-session/gsm-xsmp-client.c:1204
msgid "This program is blocking logout."
msgstr "Այս ծրագիրն արգելափակում է ավարտը։"

#: ../ukui-session/gsm-xsmp-server.c:325
msgid ""
"Refusing new client connection because the session is currently being shut "
"down\n"
msgstr "Նոր կլիենտի կապի մերժում, որովհետև նիստը ներկայումս փակվում է\n"

#: ../ukui-session/gsm-xsmp-server.c:587
#, c-format
msgid "Could not create ICE listening socket: %s"
msgstr "Չհաջողվեց ստեղծել ICE լսելու սոկետ: %s"

#. Oh well, no X for you!
#: ../ukui-session/gsm-util.c:356
#, c-format
msgid "Unable to start login session (and unable to connect to the X server)"
msgstr "Անհմար է սկսվել մուտքը դեպի համակարգ (և անհնար է միանալ X սերվերին)"

#: ../ukui-session/main.c:639
msgid "Override standard autostart directories"
msgstr "Չեղյալ հայտարարել ստանդարտ ինքնաթողք տեղեկագրքերը"

#: ../ukui-session/main.c:640
msgid "Enable debugging code"
msgstr "Ակտրվացնել ծածկագրի վրիպակազերծումը"

#: ../ukui-session/main.c:641
msgid "Do not load user-specified applications"
msgstr "Չբեռնել օգտվողի նշված դիմումները"

#: ../ukui-session/main.c:643
msgid "Show the keyboard shortcuts"
msgstr ""

#: ../ukui-session/main.c:663
#, fuzzy
msgid " - the UKUI session manager"
msgstr "- MATE աշխատաշրջանի կառավարիչը"

#: ../ukui-session/uksm-shortcuts-dialog.c:350
#, fuzzy, c-format
msgid "This window will be automatically closed in %d second"
msgid_plural "This window will be automatically closed in %d seconds"
msgstr[0] "Դուք պետք է ավտոմատ կերպով դուրս գաք %d վայրկյանում"
msgstr[1] "Դուք պետք է ավտոմատ կերպով դուրս գաք %d վայրկյաններում"

#: ../ukui-session/uksm-shortcuts-dialog.c:466
msgid "Keyboard Shortcuts"
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:468
#, fuzzy
msgid "Application"
msgstr "Գործարկման Դիմումներ"

#: ../ukui-session/uksm-shortcuts-dialog.c:469
msgid "Super"
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:470
msgid "Open start menu."
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:471
msgid "Ctrl+Alt+T"
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:472
msgid "Open termianl."
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:473
msgid "Ctrl+Alt+D"
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:474
msgid "Show desktop."
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:475
msgid "Ctrl+Alt+L"
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:476
#, fuzzy
msgid "Lock screen."
msgstr "Արգելափակել էկրանը"

#. Separator
#: ../ukui-session/uksm-shortcuts-dialog.c:479
#: ../ukui-session/uksm-shortcuts-dialog.c:500
msgid "  "
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:481
msgid "Screenshot"
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:482
msgid "Print"
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:483
msgid "Take a full-screen screenshot."
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:484
msgid "Ctrl+Print"
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:485
msgid "Take a screenshot of the current window."
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:486
msgid "Shift+Print"
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:487
msgid "Select area to grab."
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:489
msgid "Switching"
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:490
msgid "Alt+Tab"
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:491
#, fuzzy
msgid "Switches between applications."
msgstr "Գործարկման Դիմումներ"

#: ../ukui-session/uksm-shortcuts-dialog.c:492
msgid "Alt+Shift+Tab"
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:493
msgid "Switches between applications in reverse order."
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:494
msgid "Super_L+P/F3/F7"
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:495
msgid "Switches between display."
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:496
msgid "Ctrl+Alt+ArrowKeys"
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:497
msgid "Switches between workspaces."
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:502
msgid "Windows"
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:503
msgid "Alt+F9"
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:504
msgid "Maximize the current window."
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:505
msgid "Alt+F10"
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:506
msgid "Minimize the current window."
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:507
msgid "Alt+F5"
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:508
msgid "Cancel maximize the current window."
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:509
msgid "Alt+F4"
msgstr ""

#: ../ukui-session/uksm-shortcuts-dialog.c:510
#, fuzzy
msgid "Close the current window."
msgstr "Սահմանել ըթացիկ աշխատաշրջանի անունը"

#: ../tools/ukui-session-save.c:70
msgid "Log out"
msgstr "Դուրս գալ"

#: ../tools/ukui-session-save.c:71
msgid "Log out, ignoring any existing inhibitors"
msgstr "Դուրս գալ, անտեսելով առկա արգելակիչները"

#: ../tools/ukui-session-save.c:72
msgid "Show logout dialog"
msgstr "Ցույց տալ դուրս գալ երկխոսությունը"

#: ../tools/ukui-session-save.c:73
msgid "Show shutdown dialog"
msgstr "Ցույց տալ անջատել երկխոսությունը"

#: ../tools/ukui-session-save.c:74
msgid "Use dialog boxes for errors"
msgstr "Սխալների համար օգտագործել երկխոսության արկղեր"

#. deprecated options
#: ../tools/ukui-session-save.c:76
msgid "Set the current session name"
msgstr "Սահմանել ըթացիկ աշխատաշրջանի անունը"

#: ../tools/ukui-session-save.c:76
msgid "NAME"
msgstr "ԱՆՈՒՆ"

#: ../tools/ukui-session-save.c:77
msgid "Kill session"
msgstr "Սեանսը ոչնչացնել"

#: ../tools/ukui-session-save.c:78
msgid "Do not require confirmation"
msgstr "Չի պահանջում հաստատում"

#. Okay, it wasn't the upgrade case, so now we can give up.
#: ../tools/ukui-session-save.c:136 ../tools/ukui-session-save.c:151
msgid "Could not connect to the session manager"
msgstr "Անկարող է միանալ սեանսի մենեջերին"

#: ../tools/ukui-session-save.c:277
msgid "Program called with conflicting options"
msgstr "Ծրագիրը կանչվել է կոնֆլիտային տարբերակների հետ"

#: ../tools/ukui-session-inhibit.c:116
#, c-format
msgid ""
"%s [OPTION...] COMMAND\n"
"\n"
"Execute COMMAND while inhibiting some session functionality.\n"
"\n"
"  -h, --help        Show this help\n"
"  --version         Show program version\n"
"  --app-id ID       The application id to use\n"
"                    when inhibiting (optional)\n"
"  --reason REASON   The reason for inhibiting (optional)\n"
"  --inhibit ARG     Things to inhibit, colon-separated list of:\n"
"                    logout, switch-user, suspend, idle, automount\n"
"\n"
"If no --inhibit option is specified, idle is assumed.\n"
msgstr ""
"%s [ՏԱՐԲԵՐԱԿ...] ՀՐԱՄԱՆ\n"
"\n"
"Կատարել հրամանը մինչդեռ արգելակվում է  որոշ նիստի ֆունկցիոնալությունը։\n"
"\n"
"-h, --help Ցույց այդ օգնությունը\n"
"--version Ցույց տալ ծրագրի նոր տարբերակը\n"
"--app-id ID Դիմումի օգտագործած id\n"
"երբ արգելակել (ըստ ցանկության)\n"
"--reason ՊԱՏՃԱՌԸ Պատճառն արգելակելու (ըստ ցանկության)\n"
"--inhibit ARG Արգելակող տարրեր, կետ-առանձնացված ցանկ:\n"
"Դուրս գալ, օգտագործողի փոխանջատում, կասեցնել, պարապուրդ, ապատեղակայում\n"
"Եթե ոչ --inhibit տարբերակն է նշված, ենթադրվում է պարապ է։\n"

#: ../tools/ukui-session-inhibit.c:170 ../tools/ukui-session-inhibit.c:180
#: ../tools/ukui-session-inhibit.c:190
#, c-format
msgid "%s requires an argument\n"
msgstr "%s պահանջում է արգումենտ\n"

#: ../tools/ukui-session-inhibit.c:226
#, c-format
msgid "Failed to execute %s\n"
msgstr "Չհաջողվեց կատարել %s\n"

#~ msgid "MATE"
#~ msgstr "MATE"
